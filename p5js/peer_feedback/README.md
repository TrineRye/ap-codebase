# Peer feedback questions

## miniX01

✏️ *Describe*:
- What do you see? Can you explain what the program is about?
- How was is made? What part of the code do you find the most exciting and why?

✨ *Reflect*:
- Is it successful? Does it explore the prompt in a compelling, interesting, or unique way?
- How is this approach/way of thinking different from your own miniX?
- What are the differences between reading other people’s code and writing your own code? What can you learn from reading others’ works?

---

## miniX02

✏️ *Describe*:
- What do you see? Can you explain what the program is about?
- How was is made? What part of the code do you find the most exciting and why

✨ *Reflect*:
- Is it successful? Does it explore the prompt in a compelling, interesting, or unique way?
- How is this approach/way of thinking different from your own miniX?
- Has the work addressed the aesthetics/politics of representation conceptually or visually? - How?

---

## miniX03

✏️ *Describe*:
- What is the program about?
- What syntaxes were used?
- What does the work express?
- Does it link to the assigned readings explicitly? Conceptual linkage / reflection beyond the technical


✨ *Reflect*:
- Do you resonate with the design of the program, and why?
- Which aspect(s) do you like the most?
- How would you interpret the work, and how’s the thinking different from your own miniX?
- Can you relate the work to some of the concepts/quotes in texts?

---

## miniX04

✏️ *Describe*:
- What is the program about?
- What syntaxes were used?
- What data have been captured?
- Does it link to any of the assigned readings explicitly? If yes, how?

✨ *Reflect*:
- Is it successful? Does it explore the prompt in a compelling, interesting, or unique way?
- Which aspect(s) do you like the most?
- How would you interpret the work, and how’s the thinking different from your own miniX?
- How does the work change or inform your understanding of data capture?

---

## miniX05

✏️ *Describe*:

✨ *Reflect*:

---

## miniX06

✏️ *Describe*:

✨ *Reflect*:

---

## miniX07

✏️ *Describe*:

✨ *Reflect*:

---

## miniX08

✏️ *Describe*:

✨ *Reflect*:

---

## miniX09

✏️ *Describe*:

✨ *Reflect*:

---

## miniX10

✏️ *Describe*:

✨ *Reflect*:

---